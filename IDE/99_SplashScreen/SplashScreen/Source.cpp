#include "windows.h"


bool gbFullScreen = false;
HWND ghwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) }; 

LRESULT CALLBACK WndProc(HWND hwnd, UINT imsg, WPARAM wparam, LPARAM lparam);

void ToggleFullScreen(void)
{
	//Variable Declerations
	MONITORINFO mi;

	//code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(false);
	}
	else
	{
		//code 
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}


int WINAPI WinMain(HINSTANCE hinstance, HINSTANCE hprevInstance, LPSTR cmdline, int icmdshow)
{
	WNDCLASSEX wndclass;
	ZeroMemory(&wndclass, sizeof(WNDCLASSEX));

	TCHAR Appname[] = TEXT("abc");

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpszClassName = Appname;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hInstance = hinstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszMenuName = NULL;

	int retval = RegisterClassEx(&wndclass);

	ghwnd = CreateWindow(Appname,
		TEXT("Native Window"), 
		WS_OVERLAPPEDWINDOW,
		0, 
		0, 
		800, 
		500, 
		NULL, 
		NULL, 
		hinstance,
		NULL);

	ShowWindow(ghwnd, SW_NORMAL);
	UpdateWindow(ghwnd);

	ToggleFullScreen();
	gbFullScreen = true;

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);	
	}

    return msg.wParam;
}






HANDLE imgHndl = NULL;
float imgx =0;
float imgy =0;

LRESULT CALLBACK WndProc(HWND hwnd, UINT imsg, WPARAM wparam, LPARAM lparam)
{
	PAINTSTRUCT ps;
	HDC hDC;
	HDC hMemDC;
	RECT rcClient;
	BITMAP bmp;


	switch (imsg)
	{
	case WM_CREATE:
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_PAINT:
		imgHndl = LoadImageA(NULL,"1.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
		GetObject (imgHndl, sizeof (bmp), &bmp);
		imgx = bmp.bmWidth;
		imgy = bmp.bmHeight;
		GetClientRect (hwnd, &rcClient);

		hDC = BeginPaint (hwnd, &ps);
		hMemDC = CreateCompatibleDC (hDC);
		SelectObject(hMemDC, imgHndl);
		StretchBlt (hDC, 0, 0, rcClient.right, rcClient.bottom, hMemDC, 0, 0, imgx, imgy, SRCCOPY);
		break;

	case WM_KEYDOWN:
		switch (wparam)
		{
		case 0x46: // F
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;
		}
	default:
		break;
	}

	return DefWindowProc(hwnd, imsg, wparam, lparam);
}
