#include "windows.h"
#include "resource.h"


#define PC_PROPERTY 8
#define PC_PART 12

HWND ghwnd;
int cxChar, cyChar, cxCaps;
TEXTMETRIC tm;

LRESULT CALLBACK WndProc(HWND hwnd, UINT imsg, WPARAM wparam, LPARAM lparam);
BOOL CALLBACK MyDlgProc(HWND, UINT, WPARAM, LPARAM);
void DisplayTabularData();

#define CPU_COMPANY_SIZE				3
#define RAM_COMPANY_SIZE				4
#define MOTHERBOARD_COMPANY_SIZE		4
#define GRAPHICSCARD_COMPANY_SIZE		3
#define HARDDISK_COMPANY_SIZE			3
#define CDDRIVE_COMPANY_SIZE			3
#define SMPS_COMPANY_SIZE				3
#define KEYBOARD_COMPANY_SIZE			3
#define MOUSE_COMPANY_SIZE				3
#define MONITER_COMPANY_SIZE			3
#define PRINTER_COMPANY_SIZE			3
#define CABINET_COMPANY_SIZE			4

#define HARDDISK_SIZE_SIZE			    4
#define RAM_SIZE_SIZE					4
#define GRAPHICSCARD_SIZE_SIZE			4
#define MONITER_SIZE_SIZE				4


struct SelectedIndices
{
	int CPUIndex;
	int RAMINdex; 
	int MotherboardIndex;
	int GraphicsCardIndex;
	int HardDiskIndex;
	int CDDVDDriveIndex;
	int SMPSIndex;
	int CabinetIndex;
	int KeyboardIndex;
	int MouseIndex;
	int MoniterIndex;
	int PrinterIndex;	
};

void OnClickPurchase(HWND hDlgwnd);

#pragma region Data Structures

TCHAR * CPUCompanyList[CPU_COMPANY_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("Intel"),
	TEXT("AMD")
};

TCHAR * RAMCompanyList[RAM_COMPANY_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("G.Skill"),  
	TEXT("Corsair"),
	TEXT("Dell")
};

TCHAR * MotherBoardCompanyList[MOTHERBOARD_COMPANY_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("INTEL DH67BL"),
	TEXT("ASUS PRIME - B250"),
	TEXT("Acer MCP73T - AD")
};

TCHAR * GraphicsCardCompanyList[GRAPHICSCARD_COMPANY_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("NVDIA"),
	TEXT("ATI")
};

TCHAR * HardDiskCompanyList[HARDDISK_COMPANY_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("Western Digital"),
	TEXT("Segate")
};

TCHAR * CDDriveCompanyList[CDDRIVE_COMPANY_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("LG "),
	TEXT("Samsung")
};

TCHAR * SMPSCompanyList[SMPS_COMPANY_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("Antec"),
	TEXT("Corsair")
};

TCHAR * KeyboardCompanyList[KEYBOARD_COMPANY_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("Logitech"),
	TEXT("Microsoft")
};

TCHAR * MouseCompanyList[MOUSE_COMPANY_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("Logitech"),
	TEXT("Microsoft")
};

TCHAR * MoniterCompanyList[MONITER_COMPANY_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("Benque"),
	TEXT("Acer")
};

TCHAR * PrinterCompanyList[PRINTER_COMPANY_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("HP"),
	TEXT("DELL")
};

TCHAR * CabinetCompanyList[CABINET_COMPANY_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("Antec"),
	TEXT("Antec"),
	TEXT("Corsair")
};





TCHAR * HardDiskSizeList[HARDDISK_SIZE_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("Antec"),
	TEXT("Antec"),
	TEXT("Corsair")
};

TCHAR * RamSizeList[RAM_SIZE_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("Antec"),
	TEXT("Antec"),
	TEXT("Corsair")
};

TCHAR * GraphicsCardSizeList[GRAPHICSCARD_SIZE_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("Antec"),
	TEXT("Antec"),
	TEXT("Corsair")
};

TCHAR * MoniterSizeList[MONITER_SIZE_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("Antec"),
	TEXT("Antec"),
	TEXT("Corsair")
};

#pragma endregion

SelectedIndices selectIndex;

int WINAPI WinMain(HINSTANCE hinstance, HINSTANCE hprevInstance, LPSTR cmdline, int icmdshow)
{
	WNDCLASSEX wndclass;
	ZeroMemory(&wndclass, sizeof(WNDCLASSEX));

	TCHAR Appname[] = TEXT("abc");

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpszClassName = Appname;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hInstance = hinstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszMenuName = NULL;

	int retval = RegisterClassEx(&wndclass);

	ghwnd = CreateWindow(Appname,
		TEXT("Computer Shoppe"),
		WS_OVERLAPPEDWINDOW,
		0,
		0,
		800,
		500,
		NULL,
		NULL,
		hinstance,
		NULL);

	ShowWindow(ghwnd, SW_SHOWMAXIMIZED);
	UpdateWindow(ghwnd);

	HANDLE imgHndl = NULL;
	LONG imgx = 0;
	LONG imgy = 0;
	HDC hDC;
	HDC hMemDC;
	RECT rcClient;
	BITMAP bmp;
	imgHndl = LoadImageA(NULL, "1.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(imgHndl, sizeof(bmp), &bmp);
	imgx = bmp.bmWidth;
	imgy = bmp.bmHeight;
	GetClientRect(ghwnd, &rcClient);

	hDC = GetDC(ghwnd);
	hMemDC = CreateCompatibleDC(hDC);
	SelectObject(hMemDC, imgHndl);
	StretchBlt(hDC, 0, 0, rcClient.right, rcClient.bottom, hMemDC, 0, 0, imgx, imgy, SRCCOPY);

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT imsg, WPARAM wparam, LPARAM lparam)
{
	HINSTANCE hinst;
	switch (imsg)
	{
	case WM_CREATE:
		 break;

	case WM_DESTROY: 
		PostQuitMessage(0);
		break;

	case WM_KEYDOWN:
		switch (wparam)
		{
		case VK_SPACE:
			//for creating dialog
			ShowWindow(ghwnd, SW_HIDE);
			hinst = (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE);
			DialogBox(hinst, TEXT("MYDIALOG"), hwnd, MyDlgProc);
		default:
			break;
		}

	default:
		break;
	}

	return DefWindowProc(hwnd, imsg, wparam, lparam);
}

#pragma region combobox data population

void AddListOfCPUCompany(HWND dlgHWND)
{
	for ( int i = 0; i < CPU_COMPANY_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_CPU_COMPANY, CB_ADDSTRING, 0, (LPARAM)CPUCompanyList[i]);
	}

	SendDlgItemMessage(dlgHWND, IDCB_CPU_COMPANY, CB_SETCURSEL, 0, 0);
}

void AddListOfRAMCompany(HWND dlgHWND) 
{
	for (int i = 0; i < RAM_COMPANY_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_RAM_COMPANY, CB_ADDSTRING, 0, (LPARAM)RAMCompanyList[i]);
	}

	SendDlgItemMessage(dlgHWND, IDCB_RAM_COMPANY, CB_SETCURSEL, 0, 0);
}

void AddListOfMotherboardCompany(HWND dlgHWND) 
{
	for (int i = 0; i < MOTHERBOARD_COMPANY_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_MOTHERBOARD_COMPANY, CB_ADDSTRING, 0, (LPARAM)MotherBoardCompanyList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_MOTHERBOARD_COMPANY, CB_SETCURSEL, 0, 0);
}

void AddListOfGraphicsCardCompany(HWND dlgHWND) 
{
	for (int i = 0; i < GRAPHICSCARD_COMPANY_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_GRAPHICSCARD_COMPANY, CB_ADDSTRING, 0, (LPARAM)GraphicsCardCompanyList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_GRAPHICSCARD_COMPANY, CB_SETCURSEL, 0, 0);

}

void AddListOfHardDiskCompany(HWND dlgHWND) 
{
	for (int i = 0; i < HARDDISK_COMPANY_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_HARDDISK_COMPANY, CB_ADDSTRING, 0, (LPARAM)HardDiskCompanyList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_HARDDISK_COMPANY, CB_SETCURSEL, 0, 0);
}

void AddListOfCDDVDDriveCompany(HWND dlgHWND) 
{
	for (int i = 0; i < CDDRIVE_COMPANY_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_CDDRIVE_COMPANY, CB_ADDSTRING, 0, (LPARAM)CDDriveCompanyList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_CDDRIVE_COMPANY, CB_SETCURSEL, 0, 0);
}

void AddListOfSMPSCompany(HWND dlgHWND) 
{
	for (int i = 0; i < SMPS_COMPANY_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_SMPS_COMPANY, CB_ADDSTRING, 0, (LPARAM)SMPSCompanyList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_SMPS_COMPANY, CB_SETCURSEL, 0, 0);
}

void AddListOfCabinetCompany(HWND dlgHWND) 
{
	for (int i = 0; i < CABINET_COMPANY_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_CABINET_COMPANY, CB_ADDSTRING, 0, (LPARAM)CabinetCompanyList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_CABINET_COMPANY, CB_SETCURSEL, 0, 0);
}

void AddListOfKeyboardCompany(HWND dlgHWND) 
{
	for (int i = 0; i < KEYBOARD_COMPANY_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_KEYBOARD_COMPANY, CB_ADDSTRING, 0, (LPARAM)KeyboardCompanyList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_KEYBOARD_COMPANY, CB_SETCURSEL, 0, 0);
}

void AddListOfMouseCompany(HWND dlgHWND) 
{
	for (int i = 0; i < MOUSE_COMPANY_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_MOUSE_COMPANY, CB_ADDSTRING, 0, (LPARAM)MouseCompanyList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_MOUSE_COMPANY, CB_SETCURSEL, 0, 0);
}

void AddListOfMoniterCompany(HWND dlgHWND)
{
	for (int i = 0; i < MONITER_COMPANY_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_MONITER_COMPANY, CB_ADDSTRING, 0, (LPARAM)MoniterCompanyList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_MONITER_COMPANY, CB_SETCURSEL, 0, 0);
}

void AddListOfPrinterCompany(HWND dlgHWND) 
{
	for (int i = 0; i < PRINTER_COMPANY_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_PRINTER_COMPANY, CB_ADDSTRING, 0, (LPARAM)PrinterCompanyList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_PRINTER_COMPANY, CB_SETCURSEL, 0, 0);
}

#pragma endregion

#pragma region Dialog Event Handler

void OnClickPurchase(HWND hDlgwnd)
{
	HWND parentHnd = (HWND)GetWindowLong(hDlgwnd, GWL_HWNDPARENT);
	EndDialog(hDlgwnd, 0);
	ShowWindow(parentHnd, SW_SHOWMAXIMIZED);
	DisplayTabularData();
}

void OnSelectionChangeCPUCompany(HWND dlgHWND)
{

}

void OnSelectionChangeRAMCompany(HWND dlgHWND)
{
	
}

void OnSelectionChangeMotherboardCompany(HWND dlgHWND) 
{

}

void OnSelectionChangeGraphicsCardCompany(HWND dlgHWND) 
{

}

void OnSelectionChangeHardDiskCompany(HWND dlgHWND) 
{

}

void OnSelectionChangeCDDVDDriveCompany(HWND dlgHWND) 
{

}

void OnSelectionChangeSMPSCompany(HWND dlgHWND) 
{

}

void OnSelectionChangeCabinetCompany(HWND dlgHWND) 
{

}

void OnSelectionChangeKeyboardCompany(HWND dlgHWND) 
{

}

void OnSelectionChangeMouseCompany(HWND dlgHWND) 
{

}

void OnSelectionChangeMoniterCompany(HWND dlgHWND) 
{

}

void OnSelectionChangePrinterCompany(HWND dlgHWND) 
{

}

#pragma endregion

BOOL CALLBACK MyDlgProc(HWND hDlgwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	HANDLE imgHndl = NULL;
	LONG imgx = 0;
	LONG imgy = 0;
	PAINTSTRUCT ps;
	HDC hDC;
	HDC hMemDC;
	RECT rcClient;
	BITMAP bmp;

	HWND parentHnd = NULL;


	switch (msg)
	{
	case WM_INITDIALOG:
	{
		AddListOfCPUCompany(hDlgwnd);
		AddListOfRAMCompany(hDlgwnd);
		AddListOfMotherboardCompany(hDlgwnd);
		AddListOfGraphicsCardCompany(hDlgwnd);
		AddListOfHardDiskCompany(hDlgwnd);
		AddListOfCDDVDDriveCompany(hDlgwnd);
		AddListOfSMPSCompany(hDlgwnd);
		AddListOfCabinetCompany(hDlgwnd);
		AddListOfKeyboardCompany(hDlgwnd);
		AddListOfMouseCompany(hDlgwnd);
		AddListOfMoniterCompany(hDlgwnd);
		AddListOfPrinterCompany(hDlgwnd);
		
		ShowWindow(hDlgwnd, SW_SHOWMAXIMIZED);
	}
	return TRUE;

	case WM_CTLCOLORSTATIC:
	{
		HFONT hFont = CreateFont(22, 0, 0, 0, FW_BOLD, FALSE, FALSE, FALSE, DEFAULT_CHARSET, 0,
			CLIP_DEFAULT_PRECIS, 0, 0, TEXT("StaticTextFont")); //2nd arg 0 i.e calculate width based on aspect ratio

		static HBRUSH hbrBkgnd = NULL;
		HDC hdcStatic = (HDC)wParam;
		SetTextColor(hdcStatic, RGB(255, 255, 255));
		SelectObject(hdcStatic, hFont);
		SetBkMode(hdcStatic, TRANSPARENT);

		if (hbrBkgnd == NULL)
		{
			
			hbrBkgnd = (HBRUSH)GetStockObject(HOLLOW_BRUSH);
		}
		return (INT_PTR)hbrBkgnd;

	}
	return TRUE;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
			case IDCB_CPU_COMPANY:
				switch (HIWORD(wParam))
				{
					case CBN_SELCHANGE: 
						OnSelectionChangeCPUCompany(hDlgwnd);
						return TRUE;
				}
				break;

			case IDCB_RAM_COMPANY:
				switch (HIWORD(wParam))
				{
				case CBN_SELCHANGE:
					OnSelectionChangeRAMCompany(hDlgwnd);
					return TRUE;
				}
				break;

			case IDCB_MOTHERBOARD_COMPANY:
				switch (HIWORD(wParam))
				{
				case CBN_SELCHANGE:
					OnSelectionChangeMotherboardCompany(hDlgwnd);
					return TRUE;
				}
				break;

			case IDCB_GRAPHICSCARD_COMPANY:
				switch (HIWORD(wParam))
				{
				case CBN_SELCHANGE:
					OnSelectionChangeGraphicsCardCompany(hDlgwnd);
					return TRUE;
				}
				break;

			case IDCB_HARDDISK_COMPANY:
				switch (HIWORD(wParam))
				{
				case CBN_SELCHANGE:
					OnSelectionChangeHardDiskCompany(hDlgwnd);
					return TRUE;
				}
				break;

			case IDCB_CDDRIVE_COMPANY:
				switch (HIWORD(wParam))
				{
				case CBN_SELCHANGE:
					OnSelectionChangeCDDVDDriveCompany(hDlgwnd);
					return TRUE;
				}
				break;

			case IDCB_SMPS_COMPANY:
				switch (HIWORD(wParam))
				{
				case CBN_SELCHANGE:
					OnSelectionChangeSMPSCompany(hDlgwnd);
					return TRUE;
				}
				break;

			case IDCB_KEYBOARD_COMPANY:
				switch (HIWORD(wParam))
				{
				case CBN_SELCHANGE:
					OnSelectionChangeKeyboardCompany(hDlgwnd);
					return TRUE;
				}
				break;

			case IDCB_MOUSE_COMPANY:
				switch (HIWORD(wParam))
				{
				case CBN_SELCHANGE:
					OnSelectionChangeMouseCompany(hDlgwnd);
					return TRUE;
				}
				break;

			case IDCB_MONITER_COMPANY:
				switch (HIWORD(wParam))
				{
				case CBN_SELCHANGE:
					OnSelectionChangeMoniterCompany(hDlgwnd);
					return TRUE;
				}
				break;

			case IDCB_PRINTER_COMPANY:
				switch (HIWORD(wParam))
				{
				case CBN_SELCHANGE:
					OnSelectionChangePrinterCompany(hDlgwnd);
					return TRUE;
				}
				break;

			case IDCB_CABINET_COMPANY:
				switch (HIWORD(wParam))
				{
				case CBN_SELCHANGE:
					OnSelectionChangeCabinetCompany(hDlgwnd);
					return TRUE;
				}
				break;

			case IDBUTTON_PURCHASE:
				OnClickPurchase(hDlgwnd);
				break;
		}
		return TRUE;

	//case WM_CTLCOLORDLG:
	//	hbr = CreateSolidBrush(RGB(0, 0, 0));
	//	return ((BOOL)hbr);

	case WM_PAINT:
		imgHndl = LoadImageA(NULL, "2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		GetObject(imgHndl, sizeof(bmp), &bmp);
		imgx = bmp.bmWidth;
		imgy = bmp.bmHeight;
		GetClientRect(hDlgwnd, &rcClient);

		hDC = BeginPaint(hDlgwnd, &ps);
		hMemDC = CreateCompatibleDC(hDC);
		SelectObject(hMemDC, imgHndl);
		StretchBlt(hDC, 0, 0, rcClient.right, rcClient.bottom, hMemDC, 0, 0, imgx, imgy, SRCCOPY);
		break;
	}
	return FALSE;
}

#pragma region Displaying Data in tables

void DrawLine(HDC hdc);
void DisplayDeviceText(HDC hdc);

TCHAR * Heading[] =
{
	L"DEVICE",
	L"COMPANY",
	L"SUB-TYPE",
	L"GENERATION",
	L"SIZE",
	L"MODEL",
	L"INCHES",
	L"PRICE"
};

TCHAR* Devices[] =
{
	L"01]    CPU",
	L"02]    RAM",
	L"03]    Motherboard",
	L"04]    Graphics Card",
	L"05]    Hard Disk",
	L"06]    CD/DVD Drive",
	L"07]    SMPS",
	L"08]    Cabinet",
	L"09]    Keyboard",
	L"10]    Mouse",
	L"11]    Moniter",
	L"12]    Printer"
};

void DisplayTabularData()
{
	HPEN myPen;
	HDC hdc;

	myPen = CreatePen(PS_SOLID, 2, RGB(200, 200, 0));
	hdc = GetDC(ghwnd);
	SelectObject(hdc, myPen);
	DrawLine(hdc);
	DisplayDeviceText(hdc);
	ReleaseDC(ghwnd, hdc);
}

void DrawLine(HDC hdc)
{
	MoveToEx(hdc, 0, 50, NULL); // Horizontal line 1
	LineTo(hdc, 1525, 50);

	MoveToEx(hdc, 0, 80, NULL); // Horizontal line 2
	LineTo(hdc, 1525, 80);
	
	MoveToEx(hdc, 200, 50, NULL); // Vertical line 1
	LineTo(hdc, 200, 500);

	MoveToEx(hdc, 425, 50, NULL); // Vertical line 2
	LineTo(hdc, 425, 500);

	MoveToEx(hdc, 575, 50, NULL); // Vertical line 3
	LineTo(hdc, 575, 500);

	MoveToEx(hdc, 750, 50, NULL); // Vertical line 4
	LineTo(hdc, 750, 500);

	MoveToEx(hdc, 850, 50, NULL); // Vertical line 5
	LineTo(hdc, 850, 500);

	MoveToEx(hdc, 950, 50, NULL); // Vertical line 6
	LineTo(hdc, 950, 500);

	MoveToEx(hdc, 1055, 50, NULL); // Vertical line 7
	LineTo(hdc, 1055, 530);

	MoveToEx(hdc, 1255, 50, NULL); // Vertical line 7
	LineTo(hdc, 1255, 530);

	MoveToEx(hdc, 0, 500, NULL); // Horizonatl line 3
	LineTo(hdc, 1525, 500);

	MoveToEx(hdc, 0, 530, NULL); // Horizonatl line 4
	LineTo(hdc, 1525, 530);
}

void DisplayDeviceText(HDC hdc)
{
	//Writing Headings

	HFONT hFont = CreateFont(25, 0, 0, 0, FW_BOLD, FALSE, FALSE, FALSE, DEFAULT_CHARSET, 0,
		CLIP_DEFAULT_PRECIS, 0, 0, TEXT("HeadingFont")); //2nd arg 0 i.e calculate width based on aspect ratio

	SelectObject(hdc, hFont);

	SetTextColor(hdc, RGB(0, 200, 0));

	SetBkColor(hdc, RGB(0, 0, 0));
	int x = 50, y = 55;

	TextOut(hdc, x, y, Heading[0], wcslen(Heading[0]));
	
	x += 200;
	TextOut(hdc, x, y, Heading[1], wcslen(Heading[1]));
	
	x += 200;
	TextOut(hdc, x, y, Heading[2], wcslen(Heading[2]));
	
	x += 145;
	TextOut(hdc, x, y, Heading[3], wcslen(Heading[3]));
	
	x += 175;
	TextOut(hdc, x, y, Heading[4], wcslen(Heading[4]));
	
	x += 95;
	TextOut(hdc, x, y, Heading[5], wcslen(Heading[5]));
	
	x += 100;
	TextOut(hdc, x, y, Heading[6], wcslen(Heading[6]));
	
	x += 150;
	TextOut(hdc, x, y, Heading[7], wcslen(Heading[7]));
	
	DeleteObject(hFont);

	hFont = CreateFont(22, 0, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, DEFAULT_CHARSET, 0,
						CLIP_DEFAULT_PRECIS, 0, 0, TEXT("DeviceFont")); //2nd arg 0 i.e calculate width based on aspect ratio

	SelectObject(hdc, hFont);
	SetTextColor(hdc, RGB(200, 36, 213));
	SetBkColor(hdc, RGB(0, 0, 0));

	x = 10; y = 100;
	for (int i = 0; i < PC_PART; i++)
	{
		TextOut(hdc, x, y, Devices[i], wcslen(Devices[i]));
		y = y + 30;
	}
	DeleteObject(hFont);

}

#pragma endregion