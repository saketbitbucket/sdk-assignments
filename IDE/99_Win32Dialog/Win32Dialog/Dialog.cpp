#include "windows.h"
#include "resource.h"


#define PC_PROPERTY 8
#define PC_PART 12

HWND ghwnd;
int cxChar, cyChar, cxCaps;
TEXTMETRIC tm;

LRESULT CALLBACK WndProc(HWND hwnd, UINT imsg, WPARAM wparam, LPARAM lparam);
BOOL CALLBACK MyDlgProc(HWND, UINT, WPARAM, LPARAM);

TCHAR * properties[] = 
{ 
L"DEVICE",
L"COMPANY",
L"SUB-TYPE",
L"GENERATION",
L"SIZE",
L"MODEL",
L"INCHES",
L"PRICE"
};

TCHAR* parts[] =
{ 
L"01]    CPU",
L"02]    RAM",
L"03]    Motherboard",
L"04]    Graphics Card",
L"05]    Hard Disk",
L"06]    CD/DVD Drive",
L"07]    SMPS",
L"08]    Cabinet",
L"09]    Keyboard",
L"10]    Mouse",
L"11]    Moniter",
L"12]    Printer"
};


int WINAPI WinMain(HINSTANCE hinstance, HINSTANCE hprevInstance, LPSTR cmdline, int icmdshow)
{
	WNDCLASSEX wndclass;
	ZeroMemory(&wndclass, sizeof(WNDCLASSEX));

	TCHAR Appname[] = TEXT("abc");

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpszClassName = Appname;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hInstance = hinstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszMenuName = NULL;

	int retval = RegisterClassEx(&wndclass);

	ghwnd = CreateWindow(Appname, 
		TEXT("Native Window"), 
		WS_OVERLAPPEDWINDOW,
		0, 
		0, 
		800, 
		500, 
		NULL, 
		NULL, 
		hinstance,
		NULL);

	ShowWindow(ghwnd, SW_SHOWMAXIMIZED);
	UpdateWindow(ghwnd);

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);	
	}

    return msg.wParam;
}

void DrawLine(HDC hdc);
void DisplayText(HDC hdc);

LRESULT CALLBACK WndProc(HWND hwnd, UINT imsg, WPARAM wparam, LPARAM lparam)
{
	HINSTANCE hinst;
	PAINTSTRUCT ps;
	HPEN myPen;
	HDC hdc;
	switch (imsg)
	{
	case WM_CREATE:
		hdc = GetDC(hwnd);
		GetTextMetrics(hdc, &tm);
		cxChar = tm.tmAveCharWidth;
		cyChar = tm.tmHeight + tm.tmExternalLeading;

		if ((tm.tmPitchAndFamily & 1) != 0)
			cxCaps = 3 * cxCaps / 2;
		else
			cxCaps = cxChar;

		//for creating dialog
		/*hinst = (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE);
		DialogBox(hinst, TEXT("MYDIALOG"), hwnd, MyDlgProc);*/
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_PAINT:
		myPen = CreatePen(PS_SOLID, 2, RGB(200, 200, 0));
		hdc = BeginPaint(hwnd,&ps);
		SelectObject(hdc, myPen);
		DrawLine(hdc);
		DisplayText(hdc);
		EndPaint(hwnd, &ps);
		break;

	default:
		break;
	}

	return DefWindowProc(hwnd, imsg, wparam, lparam);
}
 
BOOL CALLBACK MyDlgProc(HWND hDlgwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	HBRUSH hbr;
	switch (msg)
	{
	case WM_INITDIALOG:
	{
		TCHAR Planets[9][10] =
		{
			TEXT("Apple"), TEXT("Banana"), TEXT("Coconut"), TEXT("dates"),
			TEXT("lichi"), TEXT("mango"), TEXT("orange"), TEXT("papaya"),
			TEXT("leamon")
		};

		TCHAR A[16];
		int  k = 0;

		memset(&A, 0, sizeof(A));
		for (k = 0; k <= 8; k += 1)
		{
			wcscpy_s(A, sizeof(A) / sizeof(TCHAR), (TCHAR*)Planets[k]);

			// Add string to combobox.
			SendDlgItemMessage(hDlgwnd,IDCB_FIRST, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)A);
		}

		// Send the CB_SETCURSEL message to display an initial item 
		//  in the selection field  
		SendDlgItemMessage(hDlgwnd, IDCB_FIRST, CB_SETCURSEL , (WPARAM)0, (LPARAM)0);

	}
		return TRUE;

	case WM_COMMAND:
			switch (LOWORD(wParam))
			{
			case IDCANCEL:
				EndDialog(hDlgwnd, 0);
				break;

			case IDCB_FIRST:
				switch (HIWORD(wParam))
				{
				case CBN_DROPDOWN: // This means that the list is about to display
					MessageBox(NULL, TEXT("Drop Down"),
						TEXT("Display Notification"), MB_OK);
					return TRUE;

				case CBN_SELCHANGE:
					MessageBox(NULL, TEXT("edit changed"), TEXT("dsfs"), MB_OK);
					return TRUE;

				case CBN_SELENDOK:
					MessageBox(NULL, TEXT("edit update"), TEXT("sdfs"), MB_OK);
					return TRUE;
				default:
					break;
				}
				break;
			}
			return TRUE;

	case WM_CTLCOLORDLG:
		hbr = CreateSolidBrush(RGB(0, 255, 255));
		return ((BOOL)hbr);

		}  
	return FALSE;
}

void DrawLine(HDC hdc)
{
	MoveToEx(hdc, 0, 50, NULL);
	LineTo(hdc, 1525, 50);

	MoveToEx(hdc, 0, 80, NULL);
	LineTo(hdc, 1525, 80);


	MoveToEx(hdc, 200, 50, NULL);
	LineTo(hdc, 200, 500);

	MoveToEx(hdc, 700, 50, NULL);
	LineTo(hdc, 700, 500);

	MoveToEx(hdc, 800, 50, NULL);
	LineTo(hdc, 800, 500);

	MoveToEx(hdc, 950, 50, NULL);
	LineTo(hdc, 950, 500);

	MoveToEx(hdc, 1000, 50, NULL);
	LineTo(hdc, 1000, 500);

	MoveToEx(hdc, 1150, 50, NULL);
	LineTo(hdc, 1150, 500);

	MoveToEx(hdc, 1200, 50, NULL);
	LineTo(hdc, 1200, 500);

	MoveToEx(hdc, 1300, 50, NULL);
	LineTo(hdc, 1300, 500);

	MoveToEx(hdc, 1400, 50, NULL);
	LineTo(hdc, 1400, 500);

	MoveToEx(hdc, 0, 500, NULL);
	LineTo(hdc, 1525, 500);

	MoveToEx(hdc, 0, 530, NULL);
	LineTo(hdc, 1525, 530);
}

void DisplayText(HDC hdc)
{
	//Writing Headings

	HFONT hFont = CreateFont(25, 0, 0, 0, FW_BOLD, FALSE, FALSE, FALSE, DEFAULT_CHARSET, 0,
		CLIP_DEFAULT_PRECIS, 0, 0, TEXT("HeadingFont")); //2nd arg 0 i.e calculate width based on aspect ratio
	
	SelectObject(hdc, hFont);

	SetTextColor(hdc, RGB(0, 200,0));

	SetBkColor(hdc, RGB(0, 0, 0));
	int x = 50, y = 55;

	for (int i = 0; i < PC_PROPERTY; i++)
	{
		TextOut(hdc, x, y, properties[i], wcslen(properties[i]));
		x = x + 200;
	}
	DeleteObject(hFont);

	
	hFont = CreateFont(22, 0, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, DEFAULT_CHARSET, 0,
		CLIP_DEFAULT_PRECIS, 0, 0, TEXT("HeadingFont")); //2nd arg 0 i.e calculate width based on aspect ratio

	SelectObject(hdc, hFont);
	SetTextColor(hdc, RGB(200, 36, 213));
	SetBkColor(hdc, RGB(0, 0, 0));

	x = 5; y = 100;
	for (int i = 0; i < PC_PART; i++)
	{
		TextOut(hdc, x, y, parts[i], wcslen(parts[i]));
		y = y + 30;
	}
	DeleteObject(hFont);

}