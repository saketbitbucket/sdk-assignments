#include "windows.h"
#include "resource.h"
#include "stdio.h"

#define PC_PROPERTY 8
#define PC_PART 12

HWND ghwnd;
int cxChar, cyChar, cxCaps;
TEXTMETRIC tm;

LRESULT CALLBACK WndProc(HWND hwnd, UINT imsg, WPARAM wparam, LPARAM lparam);
BOOL CALLBACK MyDlgProc(HWND, UINT, WPARAM, LPARAM);

void DisplayStaticTabularData();
void DisplayCompanyData();
void DisplayFixedText(HDC hdc);
void DisplaySUbTypeData();
void DisplayGenerationData();
void DisplaySizeData();
void DisplayModelData();
void DisplayInchesData();
void DisplayPriceData();

#define CPU_COMPANY_SIZE				3
#define RAM_COMPANY_SIZE				4
#define MOTHERBOARD_COMPANY_SIZE		4
#define GRAPHICSCARD_COMPANY_SIZE		3
#define HARDDISK_COMPANY_SIZE			3
#define CDDRIVE_COMPANY_SIZE			3
#define SMPS_COMPANY_SIZE				3
#define KEYBOARD_COMPANY_SIZE			3
#define MOUSE_COMPANY_SIZE				3
#define MONITER_COMPANY_SIZE			3
#define PRINTER_COMPANY_SIZE			3
#define CABINET_COMPANY_SIZE			4

#define HARDDISK_SIZE_SIZE			    3
#define RAM_SIZE_SIZE					4
#define GRAPHICSCARD_SIZE_SIZE			4
#define MONITER_SIZE_SIZE				3

#define INTEL_CPU_SIZE                  4
#define AMD_CPU_SIZE                    4

#define CPU_MODEl_SIZE                  3

struct SelectedIndices
{
	int CPUIndex;
	int RAMINdex;
	int MotherboardIndex;
	int GraphicsCardIndex;
	int HardDiskIndex;
	int CDDVDDriveIndex;
	int SMPSIndex;
	int CabinetIndex;
	int KeyboardIndex;
	int MouseIndex;
	int MoniterIndex;
	int PrinterIndex;

	int RAMSizeIndex;
	int GraphicsCardSizeIndex;
	int HardDiskSizeIndex;
	int MoniterSizeIndex;

	int IntelSubType;
	int AMDSubType;
};

void OnClickPurchase(HWND hDlgwnd);

#pragma region Data Structures

TCHAR * CPUCompanyList[CPU_COMPANY_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("Intel"),
	TEXT("AMD")
};

TCHAR * IntelCPUList[INTEL_CPU_SIZE] = {
	TEXT("***Select SubType***"),
	TEXT("Core i3"),
	TEXT("Core i5"),
	TEXT("Core i7")
};

TCHAR * AMDCPUList[AMD_CPU_SIZE] = {
	TEXT("***Select SubType***"),
	TEXT("Sempron"),
	TEXT("Opteron"),
	TEXT("Radeon")
};

TCHAR * RAMCompanyList[RAM_COMPANY_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("G.Skill"),
	TEXT("Corsair"),
	TEXT("Dell")
};

TCHAR * MotherBoardCompanyList[MOTHERBOARD_COMPANY_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("INTEL DH67BL"),
	TEXT("ASUS PRIME-B250"),
	TEXT("Acer MCP73T-AD")
};

TCHAR * GraphicsCardCompanyList[GRAPHICSCARD_COMPANY_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("NVDIA"),
	TEXT("ATI")
};

TCHAR * HardDiskCompanyList[HARDDISK_COMPANY_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("Western Digital"),
	TEXT("Segate")
};

TCHAR * CDDriveCompanyList[CDDRIVE_COMPANY_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("LG "),
	TEXT("Samsung")
};

TCHAR * SMPSCompanyList[SMPS_COMPANY_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("Antec"),
	TEXT("Corsair")
};

TCHAR * KeyboardCompanyList[KEYBOARD_COMPANY_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("Logitech"),
	TEXT("Microsoft")
};

TCHAR * MouseCompanyList[MOUSE_COMPANY_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("Logitech"),
	TEXT("Microsoft")
};

TCHAR * MoniterCompanyList[MONITER_COMPANY_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("Benque"),
	TEXT("Acer")
};

TCHAR * PrinterCompanyList[PRINTER_COMPANY_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("HP"),
	TEXT("DELL")
};

TCHAR * CabinetCompanyList[CABINET_COMPANY_SIZE] = {
	TEXT("***Select Company***"),
	TEXT("Antec"),
	TEXT("Antec"),
	TEXT("Corsair")
};

TCHAR * HardDiskSizeList[HARDDISK_SIZE_SIZE] = {
	TEXT("***Select Size***"),
	TEXT("500GB"),
	TEXT("1 TB")
};

TCHAR * RamSizeList[RAM_SIZE_SIZE] = {
	TEXT("***Select Size***"),
	TEXT("2 GB"),
	TEXT("4 GB"),
	TEXT("8 GB")
};

TCHAR * GraphicsCardSizeList[GRAPHICSCARD_SIZE_SIZE] = {
	TEXT("***Select Size***"),
	TEXT("2 GB"),
	TEXT("4 GB"),
	TEXT("8 GB")
};

TCHAR * MoniterSizeList[MONITER_SIZE_SIZE] = {
	TEXT("***Select Size***"),
	TEXT("15 inch"),
	TEXT("29 inch"),
};

TCHAR * i3List[MONITER_SIZE_SIZE] = {
	TEXT("***Select Size***"),
	TEXT("4S320"),
	TEXT("4S320"),
};

TCHAR * i5List[MONITER_SIZE_SIZE] = {
	TEXT("***Select Size***"),
	TEXT("4S320"),
	TEXT("4S320"),
};

TCHAR * i7List[MONITER_SIZE_SIZE] = {
	TEXT("***Select Size***"),
	TEXT("4S320"),
	TEXT("4S320"),
};

TCHAR * SempronList[MONITER_SIZE_SIZE] = {
	TEXT("***Select Size***"),
	TEXT("4S320"),
	TEXT("4S320"),
};

TCHAR * OpteronList[MONITER_SIZE_SIZE] = {
	TEXT("***Select Size***"),
	TEXT("4S320"),
	TEXT("4S320"),
};

TCHAR * RadeonList[MONITER_SIZE_SIZE] = {
	TEXT("***Select Size***"),
	TEXT("4S320"),
	TEXT("4S320"),
};


#pragma endregion

SelectedIndices selectedindices = { 0 };

int WINAPI WinMain(HINSTANCE hinstance, HINSTANCE hprevInstance, LPSTR cmdline, int icmdshow)
{
	WNDCLASSEX wndclass;
	ZeroMemory(&wndclass, sizeof(WNDCLASSEX));

	TCHAR Appname[] = TEXT("abc");

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpszClassName = Appname;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hInstance = hinstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszMenuName = NULL;

	int retval = RegisterClassEx(&wndclass);

	ghwnd = CreateWindow(Appname,
		TEXT("Computer Shoppe"),
		WS_OVERLAPPEDWINDOW,
		0,
		0,
		800,
		500,
		NULL,
		NULL,
		hinstance,
		NULL);

	ShowWindow(ghwnd, SW_SHOWMAXIMIZED);
	UpdateWindow(ghwnd);

	HANDLE imgHndl = NULL;
	LONG imgx = 0;
	LONG imgy = 0;
	HDC hDC;
	HDC hMemDC;
	RECT rcClient;
	BITMAP bmp;
	imgHndl = LoadImage(NULL, TEXT("1.bmp"), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	GetObject(imgHndl, sizeof(bmp), &bmp);
	imgx = bmp.bmWidth;
	imgy = bmp.bmHeight;
	GetClientRect(ghwnd, &rcClient);

	hDC = GetDC(ghwnd);
	hMemDC = CreateCompatibleDC(hDC);
	SelectObject(hMemDC, imgHndl);
	StretchBlt(hDC, 0, 0, rcClient.right, rcClient.bottom, hMemDC, 0, 0, imgx, imgy, SRCCOPY);

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT imsg, WPARAM wparam, LPARAM lparam)
{
	HINSTANCE hinst;
	switch (imsg)
	{
	case WM_CREATE:
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_KEYDOWN:
		switch (wparam)
		{
		case VK_SPACE:
			//for creating dialog
			ShowWindow(ghwnd, SW_HIDE);
			hinst = (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE);
			DialogBox(hinst, TEXT("MYDIALOG"), hwnd, MyDlgProc);
		default:
			break;
		}

	default:
		break;
	}

	return DefWindowProc(hwnd, imsg, wparam, lparam);
}

#pragma region combobox data population

void AddListOfCPUCompany(HWND dlgHWND)
{
	for (int i = 0; i < CPU_COMPANY_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_CPU_COMPANY, CB_ADDSTRING, 0, (LPARAM)CPUCompanyList[i]);
	}

	SendDlgItemMessage(dlgHWND, IDCB_CPU_COMPANY, CB_SETCURSEL, 0, 0);
}

void AddListOfINTELSubType(HWND dlgHWND)
{
	for (int i = 0; i < INTEL_CPU_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_CPU_SUBTYPE, CB_ADDSTRING, 0, (LPARAM)IntelCPUList[i]);
	}

	SendDlgItemMessage(dlgHWND, IDCB_CPU_SUBTYPE, CB_SETCURSEL, 0, 0);
}

void AddListOfAMDSubType(HWND dlgHWND)
{
	for (int i = 0; i < AMD_CPU_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_CPU_SUBTYPE, CB_ADDSTRING, 0, (LPARAM)AMDCPUList[i]);
	}

	SendDlgItemMessage(dlgHWND, IDCB_CPU_SUBTYPE, CB_SETCURSEL, 0, 0);

}

void AddListOfRAMCompany(HWND dlgHWND)
{
	for (int i = 0; i < RAM_COMPANY_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_RAM_COMPANY, CB_ADDSTRING, 0, (LPARAM)RAMCompanyList[i]);
	}

	SendDlgItemMessage(dlgHWND, IDCB_RAM_COMPANY, CB_SETCURSEL, 0, 0);
}

void AddListOfMotherboardCompany(HWND dlgHWND)
{
	for (int i = 0; i < MOTHERBOARD_COMPANY_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_MOTHERBOARD_COMPANY, CB_ADDSTRING, 0, (LPARAM)MotherBoardCompanyList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_MOTHERBOARD_COMPANY, CB_SETCURSEL, 0, 0);
}

void AddListOfGraphicsCardCompany(HWND dlgHWND)
{
	for (int i = 0; i < GRAPHICSCARD_COMPANY_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_GRAPHICSCARD_COMPANY, CB_ADDSTRING, 0, (LPARAM)GraphicsCardCompanyList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_GRAPHICSCARD_COMPANY, CB_SETCURSEL, 0, 0);

}

void AddListOfHardDiskCompany(HWND dlgHWND)
{
	for (int i = 0; i < HARDDISK_COMPANY_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_HARDDISK_COMPANY, CB_ADDSTRING, 0, (LPARAM)HardDiskCompanyList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_HARDDISK_COMPANY, CB_SETCURSEL, 0, 0);
}

void AddListOfCDDVDDriveCompany(HWND dlgHWND)
{
	for (int i = 0; i < CDDRIVE_COMPANY_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_CDDRIVE_COMPANY, CB_ADDSTRING, 0, (LPARAM)CDDriveCompanyList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_CDDRIVE_COMPANY, CB_SETCURSEL, 0, 0);
}

void AddListOfSMPSCompany(HWND dlgHWND)
{
	for (int i = 0; i < SMPS_COMPANY_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_SMPS_COMPANY, CB_ADDSTRING, 0, (LPARAM)SMPSCompanyList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_SMPS_COMPANY, CB_SETCURSEL, 0, 0);
}

void AddListOfCabinetCompany(HWND dlgHWND)
{
	for (int i = 0; i < CABINET_COMPANY_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_CABINET_COMPANY, CB_ADDSTRING, 0, (LPARAM)CabinetCompanyList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_CABINET_COMPANY, CB_SETCURSEL, 0, 0);
}

void AddListOfKeyboardCompany(HWND dlgHWND)
{
	for (int i = 0; i < KEYBOARD_COMPANY_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_KEYBOARD_COMPANY, CB_ADDSTRING, 0, (LPARAM)KeyboardCompanyList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_KEYBOARD_COMPANY, CB_SETCURSEL, 0, 0);
}

void AddListOfMouseCompany(HWND dlgHWND)
{
	for (int i = 0; i < MOUSE_COMPANY_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_MOUSE_COMPANY, CB_ADDSTRING, 0, (LPARAM)MouseCompanyList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_MOUSE_COMPANY, CB_SETCURSEL, 0, 0);
}

void AddListOfMoniterCompany(HWND dlgHWND)
{
	for (int i = 0; i < MONITER_COMPANY_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_MONITER_COMPANY, CB_ADDSTRING, 0, (LPARAM)MoniterCompanyList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_MONITER_COMPANY, CB_SETCURSEL, 0, 0);
}

void AddListOfPrinterCompany(HWND dlgHWND)
{
	for (int i = 0; i < PRINTER_COMPANY_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_PRINTER_COMPANY, CB_ADDSTRING, 0, (LPARAM)PrinterCompanyList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_PRINTER_COMPANY, CB_SETCURSEL, 0, 0);
}

void AddListOfRAMSize(HWND dlgHWND)
{
	for (int i = 0; i < RAM_SIZE_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_RAM_SIZE, CB_ADDSTRING, 0, (LPARAM)RamSizeList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_RAM_SIZE, CB_SETCURSEL, 0, 0);
}


void AddListOfGraphicsCardSize(HWND dlgHWND)
{
	for (int i = 0; i < GRAPHICSCARD_SIZE_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_GRAPHICSCARD_SIZE, CB_ADDSTRING, 0, (LPARAM)GraphicsCardSizeList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_GRAPHICSCARD_SIZE, CB_SETCURSEL, 0, 0);
}


void AddListOfHardDiskSize(HWND dlgHWND)
{
	for (int i = 0; i < HARDDISK_SIZE_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_HARDDISK_SIZE, CB_ADDSTRING, 0, (LPARAM)HardDiskSizeList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_HARDDISK_SIZE, CB_SETCURSEL, 0, 0);
}


void AddListOfMonitersize(HWND dlgHWND)
{
	for (int i = 0; i < MONITER_SIZE_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_MONITER_SIZE, CB_ADDSTRING, 0, (LPARAM)MoniterSizeList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_MONITER_SIZE, CB_SETCURSEL, 0, 0);
}


void Addi3ListCpuType(HWND dlgHWND)
{
	for (int i = 0; i < CPU_MODEl_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_CPU_MODEL, CB_ADDSTRING, 0, (LPARAM)i3List[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_CPU_MODEL, CB_SETCURSEL, 0, 0);

}

void Addi5ListCpuType(HWND dlgHWND)
{
	for (int i = 0; i < CPU_MODEl_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_CPU_MODEL, CB_ADDSTRING, 0, (LPARAM)i5List[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_CPU_MODEL, CB_SETCURSEL, 0, 0);

}

void Addi7ListCpuType(HWND dlgHWND)
{
	for (int i = 0; i < CPU_MODEl_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_CPU_MODEL, CB_ADDSTRING, 0, (LPARAM)i7List[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_CPU_MODEL, CB_SETCURSEL, 0, 0);
}

void AddSempronListCpuType(HWND dlgHWND)
{
	for (int i = 0; i < CPU_MODEl_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_CPU_MODEL, CB_ADDSTRING, 0, (LPARAM)SempronList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_CPU_MODEL, CB_SETCURSEL, 0, 0);		 
}

void AddOpteronListCpuType(HWND dlgHWND)
{
	for (int i = 0; i < CPU_MODEl_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_CPU_MODEL, CB_ADDSTRING, 0, (LPARAM)OpteronList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_CPU_MODEL, CB_SETCURSEL, 0, 0);
}

void AddRadeonListCpuType(HWND dlgHWND)
{
	for (int i = 0; i < CPU_MODEl_SIZE; i++)
	{
		SendDlgItemMessage(dlgHWND, IDCB_CPU_MODEL, CB_ADDSTRING, 0, (LPARAM)RadeonList[i]);
	}
	SendDlgItemMessage(dlgHWND, IDCB_CPU_MODEL, CB_SETCURSEL, 0, 0);
}

#pragma endregion

#pragma region Dialog Event Handler

void OnClickPurchase(HWND hDlgwnd)
{
	HWND parentHnd = (HWND)GetWindowLong(hDlgwnd, GWL_HWNDPARENT);
	EndDialog(hDlgwnd, 0);

	ShowWindow(parentHnd, SW_SHOWMAXIMIZED);

	DisplayStaticTabularData();

	DisplayCompanyData();

	DisplaySUbTypeData();

	DisplayGenerationData();

	DisplaySizeData();

	DisplayModelData();

	DisplayInchesData();

	DisplayPriceData();

}

void OnSelectionChangeCPUCompany(HWND dlgHWND)
{
	selectedindices.CPUIndex = SendDlgItemMessage(dlgHWND, IDCB_CPU_COMPANY, CB_GETCURSEL, 0, 0);

	HWND hnd = GetDlgItem(dlgHWND, IDCB_CPU_SUBTYPE);

	if (selectedindices.CPUIndex < 1)
	{
		ShowWindow(hnd, SW_HIDE);
	}
	else if (selectedindices.CPUIndex == 1) //Intel
	{
		SendDlgItemMessage(dlgHWND, IDCB_CPU_SUBTYPE, CB_RESETCONTENT, 0, 0);
		AddListOfINTELSubType(dlgHWND);
		ShowWindow(hnd, SW_NORMAL);
	}
	else if (selectedindices.CPUIndex == 2) //AMD
	{
		SendDlgItemMessage(dlgHWND, IDCB_CPU_SUBTYPE, CB_RESETCONTENT, 0, 0);
		AddListOfAMDSubType(dlgHWND);
		ShowWindow(hnd, SW_NORMAL);
	}
}

void OnSelectionChangeCPUSubType(HWND dlgHWND)
{
	HWND hnd = GetDlgItem(dlgHWND, IDCB_CPU_MODEL);

	if (selectedindices.CPUIndex == 1)
	{
		selectedindices.IntelSubType = SendDlgItemMessage(dlgHWND, IDCB_CPU_SUBTYPE, CB_GETCURSEL, 0, 0);
		if (selectedindices.IntelSubType == 1)//i3
		{
			SendDlgItemMessage(dlgHWND, IDCB_CPU_MODEL, CB_RESETCONTENT, 0, 0);
			Addi3ListCpuType(dlgHWND);
			ShowWindow(hnd, SW_NORMAL);
		}
		else if (selectedindices.IntelSubType == 2)//i5
		{
			SendDlgItemMessage(dlgHWND, IDCB_CPU_MODEL, CB_RESETCONTENT, 0, 0);
			Addi5ListCpuType(dlgHWND);
			ShowWindow(hnd, SW_NORMAL);
		}
		else if (selectedindices.IntelSubType == 3)//i7
		{
			SendDlgItemMessage(dlgHWND, IDCB_CPU_MODEL, CB_RESETCONTENT, 0, 0);
			Addi7ListCpuType(dlgHWND);
			ShowWindow(hnd, SW_NORMAL);
		}
	}   
	else if (selectedindices.CPUIndex == 2)
	{
		selectedindices.AMDSubType = SendDlgItemMessage(dlgHWND, IDCB_CPU_SUBTYPE, CB_GETCURSEL, 0, 0);
		if (selectedindices.AMDSubType == 1)//i3
		{
			SendDlgItemMessage(dlgHWND, IDCB_CPU_MODEL, CB_RESETCONTENT, 0, 0);
			AddSempronListCpuType(dlgHWND);
			ShowWindow(hnd, SW_NORMAL);
		}
		else if (selectedindices.AMDSubType == 2)//i5
		{
			SendDlgItemMessage(dlgHWND, IDCB_CPU_MODEL, CB_RESETCONTENT, 0, 0);
			AddOpteronListCpuType(dlgHWND);
			ShowWindow(hnd, SW_NORMAL);
		}
		else if (selectedindices.AMDSubType == 3)//i7
		{
			SendDlgItemMessage(dlgHWND, IDCB_CPU_MODEL, CB_RESETCONTENT, 0, 0);
			AddRadeonListCpuType(dlgHWND);
			ShowWindow(hnd, SW_NORMAL);
		}
	}
	else
	{
		ShowWindow(hnd, SW_HIDE);
	}
}

void OnSelectionChangeRAMCompany(HWND dlgHWND)
{
	selectedindices.RAMINdex = SendDlgItemMessage(dlgHWND, IDCB_RAM_COMPANY, CB_GETCURSEL, 0, 0);
	
	HWND hnd = GetDlgItem(dlgHWND, IDCB_RAM_SIZE);

	if (selectedindices.RAMINdex > 0)
	{
		SendDlgItemMessage(dlgHWND, IDCB_RAM_SIZE, CB_RESETCONTENT, 0, 0);
		AddListOfRAMSize(dlgHWND);
		ShowWindow(hnd, SW_NORMAL);
	}
	else
	{
		ShowWindow(hnd, SW_HIDE);
	}
}

void OnSelectionChangeMotherboardCompany(HWND dlgHWND)
{
	selectedindices.MotherboardIndex = SendDlgItemMessage(dlgHWND, IDCB_MOTHERBOARD_COMPANY, CB_GETCURSEL, 0, 0);
}

void OnSelectionChangeGraphicsCardCompany(HWND dlgHWND)
{
	selectedindices.GraphicsCardIndex = SendDlgItemMessage(dlgHWND, IDCB_GRAPHICSCARD_COMPANY, CB_GETCURSEL, 0, 0);
}

void OnSelectionChangeHardDiskCompany(HWND dlgHWND)
{
	selectedindices.HardDiskIndex = SendDlgItemMessage(dlgHWND, IDCB_HARDDISK_COMPANY, CB_GETCURSEL, 0, 0);

	HWND hnd = GetDlgItem(dlgHWND, IDCB_HARDDISK_SIZE);

	if (selectedindices.HardDiskIndex > 0)
	{
		SendDlgItemMessage(dlgHWND, IDCB_HARDDISK_SIZE, CB_RESETCONTENT, 0, 0);
		AddListOfHardDiskSize(dlgHWND);
		ShowWindow(hnd, SW_NORMAL);
	}
	else
	{
		ShowWindow(hnd, SW_HIDE);
	}
}

void OnSelectionChangeCDDVDDriveCompany(HWND dlgHWND)
{
	selectedindices.CDDVDDriveIndex = SendDlgItemMessage(dlgHWND, IDCB_CDDRIVE_COMPANY, CB_GETCURSEL, 0, 0);
}

void OnSelectionChangeSMPSCompany(HWND dlgHWND)
{
	selectedindices.SMPSIndex = SendDlgItemMessage(dlgHWND, IDCB_SMPS_COMPANY, CB_GETCURSEL, 0, 0);
}

void OnSelectionChangeCabinetCompany(HWND dlgHWND)
{
	selectedindices.CabinetIndex = SendDlgItemMessage(dlgHWND, IDCB_CABINET_COMPANY, CB_GETCURSEL, 0, 0);
}

void OnSelectionChangeKeyboardCompany(HWND dlgHWND)
{
	selectedindices.KeyboardIndex = SendDlgItemMessage(dlgHWND, IDCB_KEYBOARD_COMPANY, CB_GETCURSEL, 0, 0);
}

void OnSelectionChangeMouseCompany(HWND dlgHWND)
{
	selectedindices.MouseIndex = SendDlgItemMessage(dlgHWND, IDCB_MOUSE_COMPANY, CB_GETCURSEL, 0, 0);
}

void OnSelectionChangeMoniterCompany(HWND dlgHWND)
{
	selectedindices.MoniterIndex = SendDlgItemMessage(dlgHWND, IDCB_MONITER_COMPANY, CB_GETCURSEL, 0, 0);

	HWND hnd = GetDlgItem(dlgHWND, IDCB_MONITER_SIZE);

	if (selectedindices.MoniterIndex > 0)
	{
		SendDlgItemMessage(dlgHWND, IDCB_MONITER_SIZE, CB_RESETCONTENT, 0, 0);
		AddListOfMonitersize(dlgHWND);
		ShowWindow(hnd, SW_NORMAL);
	}
	else
	{
		ShowWindow(hnd, SW_HIDE);
	}
}

void OnSelectionChangePrinterCompany(HWND dlgHWND)
{
	selectedindices.PrinterIndex = SendDlgItemMessage(dlgHWND, IDCB_PRINTER_COMPANY, CB_GETCURSEL, 0, 0);
}

void OnSelectionChangeRAMSize(HWND hDlgwnd)
{
	selectedindices.RAMSizeIndex = SendDlgItemMessage(hDlgwnd, IDCB_RAM_SIZE, CB_GETCURSEL, 0, 0);
}

void OnSelectionChangeGraphicsCardSize(HWND hDlgwnd)
{
	selectedindices.GraphicsCardSizeIndex = SendDlgItemMessage(hDlgwnd, IDCB_GRAPHICSCARD_SIZE, CB_GETCURSEL, 0, 0);
}

void OnSelectionChangeHardDiskSize(HWND hDlgwnd)
{
	selectedindices.HardDiskSizeIndex = SendDlgItemMessage(hDlgwnd, IDCB_HARDDISK_SIZE, CB_GETCURSEL, 0, 0);
}

void OnSelectionChangeMoniterSize(HWND hDlgwnd)
{
	selectedindices.MoniterSizeIndex = SendDlgItemMessage(hDlgwnd, IDCB_MONITER_SIZE, CB_GETCURSEL, 0, 0);
}

#pragma endregion

BOOL CALLBACK MyDlgProc(HWND hDlgwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	HANDLE imgHndl = NULL;
	LONG imgx = 0;
	LONG imgy = 0;
	PAINTSTRUCT ps;
	HDC hDC;
	HDC hMemDC;
	RECT rcClient;
	BITMAP bmp;

	HWND parentHnd = NULL;


	switch (msg)
	{
	case WM_INITDIALOG:
	{
		AddListOfCPUCompany(hDlgwnd);
		AddListOfRAMCompany(hDlgwnd);
		AddListOfMotherboardCompany(hDlgwnd);
		AddListOfGraphicsCardCompany(hDlgwnd);
		AddListOfHardDiskCompany(hDlgwnd);
		AddListOfCDDVDDriveCompany(hDlgwnd);
		AddListOfSMPSCompany(hDlgwnd);
		AddListOfCabinetCompany(hDlgwnd);
		AddListOfKeyboardCompany(hDlgwnd);
		AddListOfMouseCompany(hDlgwnd);
		AddListOfMoniterCompany(hDlgwnd);
		AddListOfPrinterCompany(hDlgwnd);

		AddListOfRAMSize(hDlgwnd);
		AddListOfMonitersize(hDlgwnd);
		AddListOfGraphicsCardSize(hDlgwnd);
		AddListOfHardDiskSize(hDlgwnd);
		ShowWindow(hDlgwnd, SW_SHOWMAXIMIZED);
	}
	return TRUE;

	case WM_CTLCOLORSTATIC:
	{
		HFONT hFont = CreateFont(22, 0, 0, 0, FW_BOLD, FALSE, FALSE, FALSE, DEFAULT_CHARSET, 0,
			CLIP_DEFAULT_PRECIS, 0, 0, TEXT("StaticTextFont")); //2nd arg 0 i.e calculate width based on aspect ratio

		static HBRUSH hbrBkgnd = NULL;
		HDC hdcStatic = (HDC)wParam;
		SetTextColor(hdcStatic, RGB(255, 255, 255));
		SelectObject(hdcStatic, hFont);
		SetBkMode(hdcStatic, TRANSPARENT);

		if (hbrBkgnd == NULL)
		{

			hbrBkgnd = (HBRUSH)GetStockObject(HOLLOW_BRUSH);
		}
		return (INT_PTR)hbrBkgnd;

	}
	return TRUE;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDCB_CPU_COMPANY:
			switch (HIWORD(wParam))
			{
			case CBN_SELCHANGE:
				OnSelectionChangeCPUCompany(hDlgwnd);
				return TRUE;
			}
			break;

		case IDCB_CPU_SUBTYPE:
			switch (HIWORD(wParam))
			{
			case CBN_SELCHANGE:
				OnSelectionChangeCPUSubType(hDlgwnd);
				return TRUE;
			}
			break;

		case IDCB_RAM_COMPANY:
			switch (HIWORD(wParam))
			{
			case CBN_SELCHANGE:
				OnSelectionChangeRAMCompany(hDlgwnd);
				return TRUE;
			}
			break;

		case IDCB_MOTHERBOARD_COMPANY:
			switch (HIWORD(wParam))
			{
			case CBN_SELCHANGE:
				OnSelectionChangeMotherboardCompany(hDlgwnd);
				return TRUE;
			}
			break;

		case IDCB_GRAPHICSCARD_COMPANY:
			switch (HIWORD(wParam))
			{
			case CBN_SELCHANGE:
				OnSelectionChangeGraphicsCardCompany(hDlgwnd);
				return TRUE;
			}
			break;

		case IDCB_HARDDISK_COMPANY:
			switch (HIWORD(wParam))
			{
			case CBN_SELCHANGE:
				OnSelectionChangeHardDiskCompany(hDlgwnd);
				return TRUE;
			}
			break;

		case IDCB_CDDRIVE_COMPANY:
			switch (HIWORD(wParam))
			{
			case CBN_SELCHANGE:
				OnSelectionChangeCDDVDDriveCompany(hDlgwnd);
				return TRUE;
			}
			break;

		case IDCB_SMPS_COMPANY:
			switch (HIWORD(wParam))
			{
			case CBN_SELCHANGE:
				OnSelectionChangeSMPSCompany(hDlgwnd);
				return TRUE;
			}
			break;

		case IDCB_KEYBOARD_COMPANY:
			switch (HIWORD(wParam))
			{
			case CBN_SELCHANGE:
				OnSelectionChangeKeyboardCompany(hDlgwnd);
				return TRUE;
			}
			break;

		case IDCB_MOUSE_COMPANY:
			switch (HIWORD(wParam))
			{
			case CBN_SELCHANGE:
				OnSelectionChangeMouseCompany(hDlgwnd);
				return TRUE;
			}
			break;

		case IDCB_MONITER_COMPANY:
			switch (HIWORD(wParam))
			{
			case CBN_SELCHANGE:
				OnSelectionChangeMoniterCompany(hDlgwnd);
				return TRUE;
			}
			break;

		case IDCB_PRINTER_COMPANY:
			switch (HIWORD(wParam))
			{
			case CBN_SELCHANGE:
				OnSelectionChangePrinterCompany(hDlgwnd);
				return TRUE;
			}
			break;

		case IDCB_CABINET_COMPANY:
			switch (HIWORD(wParam))
			{
			case CBN_SELCHANGE:
				OnSelectionChangeCabinetCompany(hDlgwnd);
				return TRUE;
			}
			break;

		case IDCB_RAM_SIZE:
			switch (HIWORD(wParam))
			{
			case CBN_SELCHANGE:
				OnSelectionChangeRAMSize(hDlgwnd);
				return TRUE;
			}
			break;

		case IDCB_GRAPHICSCARD_SIZE:
			switch (HIWORD(wParam))
			{
			case CBN_SELCHANGE:
				OnSelectionChangeGraphicsCardSize(hDlgwnd);
				return TRUE;
			}
			break;

		case IDCB_HARDDISK_SIZE:
			switch (HIWORD(wParam))
			{
			case CBN_SELCHANGE:
				OnSelectionChangeHardDiskSize(hDlgwnd);
				return TRUE;
			}
			break;

		case IDCB_MONITER_SIZE:
			switch (HIWORD(wParam))
			{
			case CBN_SELCHANGE:
				OnSelectionChangeMoniterSize(hDlgwnd);
				return TRUE;
			}
			break;

		case IDBUTTON_PURCHASE:
			OnClickPurchase(hDlgwnd);
			break;
		}
		return TRUE;

		//case WM_CTLCOLORDLG:
		//	hbr = CreateSolidBrush(RGB(0, 0, 0));
		//	return ((BOOL)hbr);

	case WM_PAINT:
		imgHndl = LoadImageA(NULL, "2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		GetObject(imgHndl, sizeof(bmp), &bmp);
		imgx = bmp.bmWidth;
		imgy = bmp.bmHeight;
		GetClientRect(hDlgwnd, &rcClient);

		hDC = BeginPaint(hDlgwnd, &ps);
		hMemDC = CreateCompatibleDC(hDC);
		SelectObject(hMemDC, imgHndl);
		StretchBlt(hDC, 0, 0, rcClient.right, rcClient.bottom, hMemDC, 0, 0, imgx, imgy, SRCCOPY);
		break;
	}
	return FALSE;
}

#pragma region Displaying Data in tables

void DrawLine(HDC hdc);
void DisplayDeviceText(HDC hdc);

TCHAR * Heading[] =
{
	L"DEVICE",
	L"COMPANY",
	L"SUB-TYPE",
	L"GENERATION",
	L"SIZE",
	L"MODEL",
	L"INCHES",
	L"PRICE"
};

TCHAR* Devices[] =
{
	L"01]    CPU",
	L"02]    RAM",
	L"03]    Motherboard",
	L"04]    Graphics Card",
	L"05]    Hard Disk",
	L"06]    CD/DVD Drive",
	L"07]    SMPS",
	L"08]    Cabinet",
	L"09]    Keyboard",
	L"10]    Mouse",
	L"11]    Moniter",
	L"12]    Printer"
};

void DisplayStaticTabularData()
{
	HPEN myPen;
	HDC hdc;

	myPen = CreatePen(PS_SOLID, 2, RGB(200, 200, 0));
	hdc = GetDC(ghwnd);
	SelectObject(hdc, myPen);
	DrawLine(hdc);
	DisplayFixedText(hdc);
	ReleaseDC(ghwnd, hdc);
}

void DrawLine(HDC hdc)
{
	MoveToEx(hdc, 0, 50, NULL); // Horizontal line 1
	LineTo(hdc, 1525, 50);

	MoveToEx(hdc, 0, 80, NULL); // Horizontal line 2
	LineTo(hdc, 1525, 80);

	MoveToEx(hdc, 200, 50, NULL); // Vertical line 1
	LineTo(hdc, 200, 500);

	MoveToEx(hdc, 425, 50, NULL); // Vertical line 2
	LineTo(hdc, 425, 500);

	MoveToEx(hdc, 575, 50, NULL); // Vertical line 3
	LineTo(hdc, 575, 500);

	MoveToEx(hdc, 750, 50, NULL); // Vertical line 4
	LineTo(hdc, 750, 500);

	MoveToEx(hdc, 850, 50, NULL); // Vertical line 5
	LineTo(hdc, 850, 500);

	MoveToEx(hdc, 950, 50, NULL); // Vertical line 6
	LineTo(hdc, 950, 500);

	MoveToEx(hdc, 1055, 50, NULL); // Vertical line 7
	LineTo(hdc, 1055, 530);

	MoveToEx(hdc, 1255, 50, NULL); // Vertical line 7
	LineTo(hdc, 1255, 530);

	MoveToEx(hdc, 0, 500, NULL); // Horizonatl line 3
	LineTo(hdc, 1525, 500);

	MoveToEx(hdc, 0, 530, NULL); // Horizonatl line 4
	LineTo(hdc, 1525, 530);
}

void DisplayFixedText(HDC hdc)
{
	//Writing Headings

	HFONT hFont = CreateFont(25, 0, 0, 0, FW_BOLD, FALSE, FALSE, FALSE, DEFAULT_CHARSET, 0,
		CLIP_DEFAULT_PRECIS, 0, 0, TEXT("HeadingFont")); //2nd arg 0 i.e calculate width based on aspect ratio

	SelectObject(hdc, hFont);

	SetTextColor(hdc, RGB(0, 200, 0));

	SetBkColor(hdc, RGB(0, 0, 0));
	int x = 50, y = 55;

	TextOut(hdc, x, y, Heading[0], wcslen(Heading[0]));

	x += 200;
	TextOut(hdc, x, y, Heading[1], wcslen(Heading[1]));

	x += 200;
	TextOut(hdc, x, y, Heading[2], wcslen(Heading[2]));

	x += 145;
	TextOut(hdc, x, y, Heading[3], wcslen(Heading[3]));

	x += 175;
	TextOut(hdc, x, y, Heading[4], wcslen(Heading[4]));

	x += 95;
	TextOut(hdc, x, y, Heading[5], wcslen(Heading[5]));

	x += 100;
	TextOut(hdc, x, y, Heading[6], wcslen(Heading[6]));

	x += 150;
	TextOut(hdc, x, y, Heading[7], wcslen(Heading[7]));

	DeleteObject(hFont);

	//Writing price text

	TextOut(hdc, 965, 505, TEXT("TOTAL"), wcslen(TEXT("TOTAL")));

	//Writing device type

	hFont = CreateFont(22, 0, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, DEFAULT_CHARSET, 0,
		CLIP_DEFAULT_PRECIS, 0, 0, TEXT("DeviceFont")); //2nd arg 0 i.e calculate width based on aspect ratio

	SelectObject(hdc, hFont);
	SetTextColor(hdc, RGB(200, 36, 213));
	SetBkColor(hdc, RGB(0, 0, 0));

	x = 10; y = 100;
	for (int i = 0; i < PC_PART; i++)
	{
		TextOut(hdc, x, y, Devices[i], wcslen(Devices[i]));
		y = y + 30;
	}
	DeleteObject(hFont);
}

void DisplayCompanyData()
{
	HDC hdc = GetDC(ghwnd);

	int x = 250, y = 100;
	TCHAR* tmp;

	tmp = (selectedindices.CPUIndex > 0) ? CPUCompanyList[selectedindices.CPUIndex] : TEXT("-");
	TextOut(hdc, x, y, tmp, wcslen(tmp));

	y = y + 30;
	tmp = (selectedindices.RAMINdex > 0) ? RAMCompanyList[selectedindices.RAMINdex] : TEXT("-");
	TextOut(hdc, x, y, tmp, wcslen(tmp));

	y = y + 30;
	tmp = (selectedindices.MotherboardIndex > 0) ? MotherBoardCompanyList[selectedindices.MotherboardIndex] : TEXT("-");
	TextOut(hdc, x, y, tmp, wcslen(tmp));

	y = y + 30;
	tmp = (selectedindices.GraphicsCardIndex > 0) ? GraphicsCardCompanyList[selectedindices.GraphicsCardIndex] : TEXT("-");
	TextOut(hdc, x, y, tmp, wcslen(tmp));

	y = y + 30;
	tmp = (selectedindices.HardDiskIndex > 0) ? HardDiskCompanyList[selectedindices.HardDiskIndex] : TEXT("-");
	TextOut(hdc, x, y, tmp, wcslen(tmp));

	y = y + 30;
	tmp = (selectedindices.CDDVDDriveIndex > 0) ? CDDriveCompanyList[selectedindices.CDDVDDriveIndex] : TEXT("-");
	TextOut(hdc, x, y, tmp, wcslen(tmp));

	y = y + 30;
	tmp = (selectedindices.SMPSIndex > 0) ? SMPSCompanyList[selectedindices.SMPSIndex] : TEXT("-");
	TextOut(hdc, x, y, tmp, wcslen(tmp));

	y = y + 30;
	tmp = (selectedindices.CabinetIndex > 0) ? CabinetCompanyList[selectedindices.CabinetIndex] : TEXT("-");
	TextOut(hdc, x, y, tmp, wcslen(tmp));

	y = y + 30;
	tmp = (selectedindices.KeyboardIndex > 0) ? KeyboardCompanyList[selectedindices.KeyboardIndex] : TEXT("-");
	TextOut(hdc, x, y, tmp, wcslen(tmp));

	y = y + 30;
	tmp = (selectedindices.MouseIndex > 0) ? MouseCompanyList[selectedindices.MouseIndex] : TEXT("-");
	TextOut(hdc, x, y, tmp, wcslen(tmp));

	y = y + 30;
	tmp = (selectedindices.MoniterIndex > 0) ? MoniterCompanyList[selectedindices.MoniterIndex] : TEXT("-");
	TextOut(hdc, x, y, tmp, wcslen(tmp));

	y = y + 30;
	tmp = (selectedindices.PrinterIndex > 0) ? PrinterCompanyList[selectedindices.PrinterIndex] : TEXT("-");
	TextOut(hdc, x, y, tmp, wcslen(tmp));
}

void DisplaySUbTypeData()
{
	HDC hdc = GetDC(ghwnd);

	int x = 450, y = 100;
	TCHAR* tmp;

	//CPU SubType
	if (selectedindices.CPUIndex == 0 ) //nothing selected from cpu company
	{
		tmp = TEXT("-");
		TextOut(hdc, x, y, tmp, wcslen(tmp));
	}
	else if (selectedindices.CPUIndex == 1) //intel
	{
		tmp = (selectedindices.IntelSubType > 0) ? IntelCPUList[selectedindices.IntelSubType] : TEXT("-");
		TextOut(hdc, x, y, tmp, wcslen(tmp));
	}
	else if (selectedindices.CPUIndex == 2) //Amd
	{
		tmp = (selectedindices.AMDSubType > 0) ? AMDCPUList[selectedindices.AMDSubType] : TEXT("-");
		TextOut(hdc, x, y, tmp, wcslen(tmp));
	}
	//CPU SubType

	y = y + 30;
	tmp = TEXT("-");
	TextOut(hdc, x, y, tmp, wcslen(tmp));

	//Graphics Card SubType
	y = y + 30;
	tmp = TEXT("-");
	TextOut(hdc, x, y, tmp, wcslen(tmp));
	//Graphics Card SubType

	for (int i = 0; i < 8; i++)
	{
		y = y + 30;
		tmp = TEXT("-");
		TextOut(hdc, x, y, tmp, wcslen(tmp));
	}
}

void DisplayGenerationData()
{
	HDC hdc = GetDC(ghwnd);

	int x = 600, y = 100;
	TCHAR* tmp;

	tmp = TEXT("-");
	TextOut(hdc, x, y, tmp, wcslen(tmp));

	y = y + 30;
	tmp = TEXT("-");
	TextOut(hdc, x, y, tmp, wcslen(tmp));

	y = y + 30;
	tmp = TEXT("-");
	TextOut(hdc, x, y, tmp, wcslen(tmp));

	y = y + 30;
	tmp = TEXT("-");
	TextOut(hdc, x, y, tmp, wcslen(tmp));

	for (int i = 0; i < 8; i++)
	{
		y = y + 30;
		tmp = TEXT("-");
		TextOut(hdc, x, y, tmp, wcslen(tmp));
	}
}

void DisplaySizeData()
{
	HDC hdc = GetDC(ghwnd);
	TCHAR* tmp = NULL;

	int x = 775, y = 100;
	for (int i = 0; i < 12; i++)
	{
		tmp = TEXT("-");
		TextOut(hdc, x, y, tmp, wcslen(tmp));
		y = y + 30;
	}

	tmp = (selectedindices.RAMSizeIndex > 0) ? RamSizeList[selectedindices.RAMSizeIndex] : TEXT("-");
	TextOut(hdc, 775, 130, tmp, wcslen(tmp));

	tmp = (selectedindices.GraphicsCardSizeIndex > 0) ? GraphicsCardSizeList[selectedindices.GraphicsCardSizeIndex] : TEXT("-");
	TextOut(hdc, 775, 190, tmp, wcslen(tmp));

	tmp = (selectedindices.HardDiskSizeIndex > 0) ? HardDiskSizeList[selectedindices.HardDiskSizeIndex] : TEXT("-");
	TextOut(hdc, 775, 220, tmp, wcslen(tmp));
}

void DisplayModelData()
{
	HDC hdc = GetDC(ghwnd);

	int x = 875, y = 100;
	for (int i = 0; i < 12; i++)
	{
		TCHAR* tmp = TEXT("-");
		TextOut(hdc, x, y, tmp, wcslen(tmp));
		y = y + 30;
	}
}

void DisplayInchesData()
{
	HDC hdc = GetDC(ghwnd);

	int x = 975, y = 100;
	for (int i = 0; i < 12; i++)
	{
		TCHAR* tmp = TEXT("-");
		TextOut(hdc, x, y, tmp, wcslen(tmp));
		y = y + 30;
	}
	TCHAR* tmp = (selectedindices.MoniterIndex > 0) ? MoniterSizeList[selectedindices.MoniterIndex] : TEXT("-");
	TextOut(hdc, 975, 400, tmp, wcslen(tmp));
}

void DisplayPriceData()
{
	HDC hdc = GetDC(ghwnd);

	int x = 1100, y = 100;
	for (int i = 0; i < 12; i++)
	{
		TCHAR* tmp = TEXT("-");
		TextOut(hdc, x, y, tmp, wcslen(tmp));
		y = y + 30;
	}
}

#pragma endregion