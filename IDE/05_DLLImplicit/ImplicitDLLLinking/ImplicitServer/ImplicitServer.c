#define UNICODE
#include <windows.h>
#include "ImplicitServer.h"

BOOL WINAPI DllMain(HANDLE hModule, DWORD dwReson, LPVOID lpReserved)
{
	switch (dwReson)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}
	return(TRUE);
}

__declspec (dllexport)int SumOfTwoIntegers(int iNum1, int iNum2)
{

	return iNum1 + iNum2;
}

__declspec (dllexport)int SubtractionOfTwoIntegers(int iNum1, int iNum2)
{

	return iNum1 - iNum2;;
}